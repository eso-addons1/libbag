---@class Bag
LibBag.Bag = ZO_Object:Subclass()

---Creates a new Bag filled with items from the specified bag id.
---Optionally, a filter function can be given that receives the slot data
---of each item. Items are then only added to the bag if the function returns true.
---@param bagId integer The bag to wrap.
---@param filter function The filter function.
function LibBag.Bag:New(bagId, filter)
    local instance = ZO_Object.New(self)
    instance:Initialize(bagId, filter)
    return instance
end

---Initializes the object.
function LibBag.Bag:Initialize(bagId, filter)
    self.bagId = bagId
    self.items = {}
    self._itemsByLink = {}
    self._previousFreeSlot = nil

    local cache = SHARED_INVENTORY:GetOrCreateBagCache(bagId)
    for slotIndex, slotData in pairs(cache) do
        if not filter or filter(slotData) then
            local item = LibBag.Item:New(bagId, slotIndex, slotData)
            self:Add(item)
        end
    end
end

---Adds an item to the bag.
---@param item Item The item to add.
function LibBag.Bag:Add(item)
    table.insert(self.items, item)
    local position = #self.items

    local link = item:Link()
    if self._itemsByLink[link] == nil then
        self._itemsByLink[link] = { position }
    else
        table.insert(self._itemsByLink[link], position)
    end
end

---Returns all items from this bag that are identical to the specified item.
---Items are considered identical if they have the same link.
---@param otherItem Item The item.
---@return table All matching items or an empty table if no items match.
function LibBag.Bag:GetMatchingItems(otherItem)
    local otherLink = otherItem:Link()
    local positions = self._itemsByLink[otherLink]
    if positions == nil then
        return {}
    else
        local result = {}
        for k, v in pairs(positions) do
            table.insert(result, self.items[v])
        end
        return result
    end
end

---Finds the next free slot in the bag.
---This works around an issue where FindFirstEmptySlotInBag always returns
---the same slot if called while items are moved - the items are actually
---moved later so the slot is still empty.
---When using this, make sure to always use the same instance of Bag.
---Returns the index of the next free slot or nil if the bag is full.
---@return integer|nil index of the next free slot
function LibBag.Bag:FindNextFreeSlot()
    if self._previousFreeSlot == nil then
        -- called the first time, get the first free slot
        self._previousFreeSlot = FindFirstEmptySlotInBag(self.bagId)
        return self._previousFreeSlot
    end

    -- find the next free slot (c.f. https://www.esoui.com/forums/showthread.php?t=2446)

    local bagSize = GetBagSize(self.bagId)
    for slotIndex = self._previousFreeSlot + 1, bagSize - 1 do
        if GetItemType(self.bagId, slotIndex) == ITEMTYPE_NONE then
            return slotIndex
        end
    end

    return nil
end
