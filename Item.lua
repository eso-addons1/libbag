---@class Item
LibBag.Item = ZO_Object:Subclass()

--- Creates a new Bag.
---@param bagId integer The bag id
---@param slotIndex integer The slot index
---@param slotData table The slot data for the item
---@return Bag
function LibBag.Item:New(bagId, slotIndex, slotData)
    local instance = ZO_Object.New(self)
    instance:Initialize(bagId, slotIndex, slotData)
    return instance
end

---Initializes the object
function LibBag.Item:Initialize(bagId, slotIndex, slotData)
    self.bagId = bagId
    self.slotIndex = slotIndex
    self.name = slotData.rawName
    self.slotData = slotData
    self.quantity, self.maxQuantity = GetSlotStackSize(bagId, slotIndex)
end

---Returns the link for the item
---@return string
function LibBag.Item:Link()
    return GetItemLink(self.bagId, self.slotIndex, LINK_STYLE_DEFAULT)
end

---Moves the item to the specified target bag and slot.
---@param targetBagId integer The target bag id.
---@param targetSlotIndex integer The target slot id.
---@param quantity integer The amount to move.
function LibBag.Item:MoveTo(targetBagId, targetSlotIndex, quantity)
    if IsProtectedFunction("RequestMoveItem") then
        CallSecureProtected("RequestMoveItem", self.bagId, self.slotIndex, targetBagId, targetSlotIndex, quantity)
    else
        RequestMoveItem(self.bagId, self.slotIndex, targetBagId, targetSlotIndex, quantity)
    end
end

---Moves any many instances of this item into an existing stack in the target bag.
---Only fills existing stacks.
---Returns the number of instances that were moved and the number of instances
---that are left in the source bag.
---@param targetBag Bag The target bag
---@return integer
---@return integer
function LibBag.Item:StackToBag(targetBag)
    local movedAmount = 0
    local remainingAmount = self.quantity

    local matchingItemsInTargetBag = targetBag:GetMatchingItems(self)
    for _, matchingItem in pairs(matchingItemsInTargetBag) do
        local remainingCapacity = matchingItem.maxQuantity - matchingItem.quantity
        if remainingCapacity > 0 then
            local amountToMove = zo_min(remainingAmount, remainingCapacity)
            self:MoveTo(targetBag.bagId, matchingItem.slotIndex, amountToMove)

            movedAmount = movedAmount + amountToMove
            remainingAmount = remainingAmount - amountToMove
        end
    end

    return movedAmount, remainingAmount
end

---Moves the whole stack into the target bag.
---This method first fills stacks of the same item in the target bag, then
---creates a new stack for the remaining items.
---Returns the number of instances that were moved and the number of instances
---that are left in the source bag.
---@param targetBag Bag The target bag
---@return integer
---@return integer
function LibBag.Item:MoveWholeStackToBag(targetBag)
    local initialQuantity = self.quantity

    -- first try to move to an existing stack of this item in the target bag
    local _, remainingAmount = self:StackToBag(targetBag)

    -- if there are still items left over in this stack, find
    -- an empty slot in the target bag and move to there
    if remainingAmount > 0 then
        local emptySlot = targetBag:FindNextFreeSlot()
        if emptySlot ~= nil then
            self:MoveTo(targetBag.bagId, emptySlot, remainingAmount)
        else
            -- target bag is full, abort
            return initialQuantity - remainingAmount, remainingAmount
        end
    end

    return initialQuantity, 0
end
