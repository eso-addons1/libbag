## APIVersion: 100033
## Title: LibBag
## Version: 1.00
## AddOnVersion: 1
## IsLibrary: true
## Author: Thenedus
## Description: A wrapper for bags and items.

LibBag.lua
Bag.lua
Item.lua

# Uncomment the following line to activate the examples
#examples.lua