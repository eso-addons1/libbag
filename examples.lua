-- Prints the name of all potions to the chat
local function ListAllPotionsFromBackpack()
    -- filtering function only returns true (i.e. match) for potions
    local function IsPotion(slotData)
        -- Note: the parameter passed to the filtering function
        -- is the raw slotData provided by the game
        return slotData.itemType == ITEMTYPE_POTION
    end

    -- creating a new Bag fills it with all items from the given bag id
    -- where the filter function returns true
    local backpack = LibBag.Bag:New(BAG_BACKPACK, IsPotion)

    -- iterate over all items in the Bag
    for _, item in pairs(backpack.items) do
        -- print the item's name
        d(item.name)
    end
end

-- Transfer an item (stack) to bank, adding it to an existing stack if possible
-- Note: The argument to this function is a LibBag.Item object
-- To get such an object, either create a LibBag.Bag object and retrieve
-- the item from there (see the function above) or use the
-- LibBag.Item:New constructor.
local function TransferToBank(item)
    -- create a Bag wrapper around the items in the bank
    -- Do not provide a filter function; the Bag must contain
    -- all items for stacking to work correctly.
    local bank = LibBag.Bag:New(BAG_BANK)
    d("Moving " .. item.name)
    local moved, remaining = item:MoveWholeStackToBag(bank)

    if (remaining == 0) then
        d("Moved successfully: " .. moved .. " items moved")
    else
        d("Move failed: " .. moved .. " of " .. (moved + remaining) .. " items moved")
    end
end

-- Register slash commands to allow running the examples
-- Use "/libbag 1" to run the first example, "/libbag 2" for the second one and so on
-- Add examples.lua to LibBag.txt!
EVENT_MANAGER:RegisterForEvent("LibBag", EVENT_ADD_ON_LOADED, function(event, addonName)
    if addonName ~= "LibBag" then return end
    EVENT_MANAGER:UnregisterForEvent("LibBag", EVENT_ADD_ON_LOADED)
    SLASH_COMMANDS["/libbag"] = function (command)
        if command == "1" then
            ListAllPotionsFromBackpack()
        elseif command == "2" then
            -- obviously, the bank must be open for this example
            -- quick hack to get any item: get the backpack and use the first item
            local backpack = LibBag.Bag:New(BAG_BACKPACK)
            local _, item = next(backpack.items)
            TransferToBank(item)
        end
    end
end)