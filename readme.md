# LibBag

A library to work with bags and items.

LibBag provides a thin wrapper around bags and items, making it easier to handle them and adding some convenience methods.

## Usage

LibBag comprises two classes, `Bag` a and `Item`. Both are available via the LibBag namespace.

## `Bag`

Constructing a new `Bag` reads and stores all items in the given ESO bag. Note that a Bag object contains a snapshot of the items that were in the bag at the time the object was created. It is not automatically updated!

### Constructor

```lua
LibBag.Bag:New(bagId, filter)
```

Creates a new bag containinng the items of the given bag id.

The `bagId` parameter defines which bag to parse, it should be given as one of the known constants, e.g. `BAG_BANK` or `BAG_BACKPACK`.

If a function is given as the optional `filter` parameter, the `slotData` of each item is passed to the given function. Each item is only added if the function returns `true`.

### Properties

`bagId` - Contains the id of the bag that the `Bag` object wraps.

`items` - A table containing all items in the bag. Each element is a `LibBag.Item` object. This property should be considered read-only; manually adding elements will cause undefined behavior - use the `Add` method to manually add items.

### Methods

`Add(item)` - Adds the given item to the bag. Usually, it is not necessary to use this method - items are automatically added when constructing the object.

`GetMatchingItems(item)` - Returns all items from the bag that are identical to the given `Item` object. Identity is determined by comparing the link or the items.

## `Item`

An `Item` object provides information of a single item contained in a `Bag`.

### Constructor

```lua
LibBag.Item:New(bagId, slotIndex, slotData)
```

Constructs a new `Item` from the provided bag id, slot index and slot data.
Usually, items are constructed from the `Bag` constructor, so calling this constructor from using code is usually not necessary.

### Properties

`bagId` - Id of the bag that contains this item.

`slotIndex` - Index of the slot that contains this item.

`name` - (Raw) name of the item.

`slotData` - The slot data for this item as provided by the game.

`quantity` - The current amount in this stack.

`maxQuantitiy` - The maximum quantity for a stack of this item.

### Methods

`Link()` - Returns the link for this item, as by the function `GetItemLink` using `LINK_STYLE_DEFAULT`.

`MoveTo(targetBagId, targetSlotIndex, quantity)` - Moves `quantity` instances of this item to the slot identified by `targetBagId` and `targetSlotIndex`. No checking is done, the caller needs to ensure that the operation is valid.

`StackToBag(targetBag)` - Moves as many instances as possible into an existing stack of the same item in the `targetBag`. Note that `targetBag` must ba a `Bag` object; do not simply pass a bag id. If not all instances in the souce `Item` fit into the matching stack, the remaining instances stay in their current slot.
Returns the amount of move instances and the amount of remaining ones.

`MoveWholeStackToBag(targetBag)` - Moves all instances of this item into the `Bag` object specified as `targetBag`. This closely mimicks the behavior of the game when for example moving an object from the backpack into the bank. First, all stacks of the same item in the `targetBag` are filled, similar to `StackToBag`. Any remaining instances are moved into an empty slot of the target bag. Returns the number of instances that could not be moved; 0 if all items were moved. No further checks are performed, the caller is responsible for checking that the item can be moved into the target bag at all. This operation is *not* atomic; if a stack in the target bag can be filled, but there is no free slot to take remaining items, the stack is filled and the remaining items stay in the source bag.

## Examples

See `examples.lua` for some simple examples of using this library. To run this library in ESO, add `examples.lua` to `LibBag.txt`. Then, run `/libbag [n]` to execute the example number `n`.

In addition, my add-on `Bankee` uses this library - see its source code for more examples.

## License

This Add-on is not created by, affiliated with or sponsored by ZeniMax Media Inc. or its affiliates. The Elder Scrolls® and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the United States and/or other countries. All rights reserved.

This add-on is licensed under the MIT license. See the LICENSE.txt file for details.
